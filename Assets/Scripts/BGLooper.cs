﻿using UnityEngine;
using System.Collections;

public class BGLooper : MonoBehaviour {
	int numBGPanels = 6;
	float pipeMax = 1.650663f;
	float pipeMin = 0.8650634f;

	void OnTriggerEnter2D (Collider2D collider) {
		float withOfBGObject = ((BoxCollider2D)collider).size.x;
		Vector3 pos = collider.transform.position;
		pos.x += withOfBGObject * numBGPanels - withOfBGObject;
		if(collider.tag == "Pipe")
			pos.y = Random.Range(pipeMin, pipeMax);
		collider.transform.position = pos;
	}
}
