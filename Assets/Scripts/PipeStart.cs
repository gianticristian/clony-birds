﻿using UnityEngine;
using System.Collections;

public class PipeStart : MonoBehaviour {
	float pipeMax = 1.650663f;
	float pipeMin = 0.8650634f;
	float aux;
	Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
		//aux = Random.Range(pipeMin, pipeMax);
		pos.y = Random.Range(pipeMin, pipeMax);
		transform.position = pos;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
