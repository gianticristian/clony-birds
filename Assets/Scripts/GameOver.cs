﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if(Input.anyKey)								// Si presiona cualquier tecla
			Application.LoadLevel("StartScreen");		// Carga la escena inicial del juego
	}
}
