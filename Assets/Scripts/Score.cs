﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	static int score = 0;
	static int highScore = 0;

	static public void AddPoint() {
		score++;
		if(score > highScore)
			highScore = score; 
	}

	void Start() {
		if(Application.loadedLevelName == "StartScreen")
			score = 0;
		highScore = PlayerPrefs.GetInt("highScore", 0);						// Toma el puntaje maximo guardado o 0 si algo salio mal
	}

	void OnDestroy() {														// Al finalizar el juego
		PlayerPrefs.SetInt("highScore", highScore);							// Guarda el puntaje maximo
	}

	void Update () {
		if(Application.loadedLevelName != "StartScreen")						// La pantalla inicial no tiene 'guiText'
			GetComponent<GUIText>().text = "Score: " + score + "\nHigh Score: " + highScore;	// Muestra el puntaje en pantalla		
	}
}
