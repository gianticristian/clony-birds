﻿using UnityEngine;
using System.Collections;

public class GroundMover : MonoBehaviour {
	Rigidbody2D player;

	void Start() {
		GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
		player = playerGO.GetComponent<Rigidbody2D>();
	}

	void FixedUpdate () {
		float vel = player.velocity.x * 0.9f;
		transform.position = transform.position + Vector3.right * vel * Time.deltaTime;

	}
}
