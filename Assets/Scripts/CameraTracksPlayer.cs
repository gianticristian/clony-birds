﻿using UnityEngine;
using System.Collections;

public class CameraTracksPlayer : MonoBehaviour {
	Transform player;
	float offsetX;			// Espaciado entre la camara y el personaje
	Vector3 pos;			// En C# no se puede poner 'transform.position.x' directamente

	// Use this for initialization
	void Start () {
		GameObject playerGO = GameObject.FindGameObjectWithTag("Player");	// Busca el jugador
		if(playerGO == null) {												// Si no existe el jugador
			Debug.LogError("Could not find the player");					// Error
			return;
		}
		player = playerGO.transform;										// Si existe guarda los datos en un game object
		offsetX = transform.position.x - player.position.x;					// Para posicionar la camara
	}
	
	// Update is called once per frame
	void Update () {
		if(player != null) {						// Si existe un jugador, o sea, no esta muerto
			pos = transform.position;				// Guarda la posicion en el vector aclarado mas arriba
			pos.x = player.position.x + offsetX;	// Calibra bien la posicion
			transform.position = pos;				// Le asigna esa posicion final a la camara
		}
	}
}
