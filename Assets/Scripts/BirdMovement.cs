﻿using UnityEngine;
using System.Collections;

public class BirdMovement : MonoBehaviour {

	float forwardSpeed = 1f;
	float flapSpeed = 100f;
	bool didFlap = false;
	Animator animator;
	bool dead = false;

	void Start() {
		animator = transform.GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	void Update() {
		if(Input.GetKeyDown(KeyCode.Space) && dead == false)							// Si apreto una tecla y no esta muerto
			didFlap = true;											
	}

	//Do physics engine updates here
	void FixedUpdate() {
		if (dead) {
			StartCoroutine(IsDead());
		}
		GetComponent<Rigidbody2D>().AddForce(Vector2.right * forwardSpeed);
		if(didFlap) {
			GetComponent<Rigidbody2D>().AddForce(Vector2.up * flapSpeed);
			animator.SetTrigger("DoFlap");
			didFlap = false;
		}
		if(GetComponent<Rigidbody2D>().velocity.y > 0) {
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
		else {
			float angle = Mathf.Lerp(0, -90, -GetComponent<Rigidbody2D>().velocity.y / 2.5f);
			transform.rotation = Quaternion.Euler(0, 0, angle);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag != "SkyLimit") {
			animator.SetTrigger("Death");
			dead = true;
		}
	}

	IEnumerator IsDead() {
		forwardSpeed = 0;
		yield return new WaitForSeconds(2);
		Application.LoadLevel("GameOver");
	}


}
